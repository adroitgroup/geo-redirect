<?php

namespace AdroitGroup\GeoRedirect;

use Illuminate\Support\Facades\Facade;

class GeoRedirectFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'geo-redirect';
    }
}
