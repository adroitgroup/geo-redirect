<?php

namespace AdroitGroup\GeoRedirect\Seeder;

use AdroitGroup\GeoRedirect\Models\Region;

class RegionSeeder
{
    public function run(): void
    {
        $columns = ['id', 'name'];
        
        $data = [
            ['015', 'Northern Africa'],
            ['021', 'Northern America'],
            ['030', 'Eastern Asia'],
            ['034', 'Southern Asia'],
            ['035', 'South-eastern Asia'],
            ['039', 'Southern Europe'],
            ['053', 'Australia and New Zealand'],
            ['054', 'Melanesia'],
            ['057', 'Micronesia'],
            ['061', 'Polynesia'],
            ['143', 'Central Asia'],
            ['145', 'Western Asia'],
            ['151', 'Eastern Europe'],
            ['154', 'Northern Europe'],
            ['155', 'Western Europe'],
            ['202', 'Sub-Saharan Africa'],
            ['419', 'Latin America and the Caribbean']
        ];

        foreach ($data as $item) {
            $mergedData = array_combine($columns, $item);

            if (! Region::where($mergedData)->first()) {
                Region::insert($mergedData);
            }
        }
    }
}