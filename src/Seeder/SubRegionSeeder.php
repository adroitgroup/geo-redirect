<?php

namespace AdroitGroup\GeoRedirect\Seeder;

use AdroitGroup\GeoRedirect\Models\SubRegion;

class SubRegionSeeder
{
    public function run(): void
    {
        $columns = ['id', 'name'];
        
        $data = [
            ['005', 'South America'],
            ['011', 'Western Africa'],
            ['013', 'Central America'],
            ['014', 'Eastern Africa'],
            ['017', 'Middle Africa'],
            ['018', 'Southern Africa'],
            ['029', 'Caribbean'],
            ['830', 'Channel Islands']
        ];

        foreach ($data as $item) {
            $mergedData = array_combine($columns, $item);

            if (! SubRegion::where($mergedData)->first()) {
                SubRegion::insert($mergedData);
            }
        }
    }
}