<?php

namespace AdroitGroup\GeoRedirect\Seeder;

use AdroitGroup\GeoRedirect\Models\Continent;

class ContinentSeeder
{
    public function run(): void
    {
        $columns = ['id', 'name'];
        
        $data = [
            ['002', 'Africa'],
            ['009', 'Oceania'],
            ['019', 'Americas'],
            ['142', 'Asia'],
            ['150', 'Europe']
        ];

        foreach ($data as $item) {
            $mergedData = array_combine($columns, $item);

            if (! Continent::where($mergedData)->first()) {
                Continent::insert($mergedData);
            }
        }
    }
}