<?php

namespace AdroitGroup\GeoRedirect\Http\Middleware;

use AdroitGroup\GeoRedirect\Services\GeoRedirectHandler;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class RedirectMiddleware
{
    private GeoRedirectHandler $geoRedirectHandler;

    private array $denyHosts;
    private array $denyPaths;

    /**
     * RedirectMiddleware constructor.
     * @param GeoRedirectHandler $geoRedirectHandler
     */
    public function __construct(GeoRedirectHandler $geoRedirectHandler)
    {
        $this->geoRedirectHandler = $geoRedirectHandler;
        $this->denyHosts = config('geo_redirect.deny_hosts', []);
        $this->denyPaths = config('geo_redirect.deny_paths', []);
    }

    public function handle(Request $request, \Closure $next)
    {
        $redirectByPass = \Config::get('geo_redirect.cookies.geo_redirect_bypass', []);

        if (is_array($redirectByPass) && count($redirectByPass) > 0) {
            foreach ($redirectByPass as $byPass) {
                if (Cookie::has($byPass) || $request->{$byPass}) {
                    return $next($request);
                }
            }
        }

        if (! $this->needRedirect($request)) {
            return $next($request);
        }

        $this->geoRedirectHandler->setBypassPath(Cookie::get('geo_path_bypass') == true);
        $this->geoRedirectHandler->setPreferredPath(Cookie::get('geo_preferred_path'));

        if ($this->redirectCondition($request)) {
            if ($this->geoRedirectHandler->isDisabled()) {
                return response(null, 422);
            }

            if ($this->geoRedirectHandler->getView()) {
                try {
                    return response()->make(view($this->geoRedirectHandler->getView()));
                } catch (BindingResolutionException $e) {}
            }

            return redirect($this->geoRedirectHandler->getUrl(), 302);
        }

        return $next($request);
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function redirectCondition(Request $request): bool
    {
        return $this->geoRedirectHandler->check($request)
            && $this->geoRedirectHandler->getUrl() !== null
            && ! in_array($request->getHost(), $this->denyHosts, true)
            && ! Str::contains($request->getUri(), $this->denyPaths)
            && ! $this->checkHost($request);
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function checkHost(Request $request): bool
    {
        if ($this->geoRedirectHandler->getHost() === '/') {
            return Str::startsWith($request->fullUrl(), $request->getSchemeAndHttpHost() . $this->geoRedirectHandler->getUrl());
        }

        return Str::startsWith($request->fullUrl(), $this->geoRedirectHandler->getUrl());
    }

    private function needRedirect(Request $request): bool
    {
        foreach (config('geo_redirect.allowed_paths', []) as $item) {
            if (preg_match($item, $request->path()) > 0) {
                return true;
            }
        }

        return false;
    }
}
