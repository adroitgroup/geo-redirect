<?php

namespace AdroitGroup\GeoRedirect\Models;

use Illuminate\Database\Eloquent\Model;

class RedirectPrefixes extends Model
{
    protected $fillable = ['redirect_id', 'prefix'];

    public $timestamps = false;
}
