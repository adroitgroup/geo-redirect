<?php

namespace AdroitGroup\GeoRedirect\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class SubRegion extends Model implements Redirectable
{
    public $timestamps = false;

    public function redirectRule(): MorphOne
    {
        return $this->morphOne(RedirectRule::class, 'redirectable');
    }
}
