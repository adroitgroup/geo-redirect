<?php

namespace AdroitGroup\GeoRedirect\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class RedirectRule extends Model
{
    protected $fillable = ['redirect_id', 'redirectable_id', 'redirectable_type'];

    public $timestamps = false;

    public function redirectable(): MorphTo
    {
        return $this->morphTo();
    }

    public function redirect(): BelongsTo
    {
        return $this->belongsTo(Redirect::class);
    }
}
