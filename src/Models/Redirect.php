<?php

namespace AdroitGroup\GeoRedirect\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Redirect extends Model
{
    public $timestamps = false;

    protected $fillable = ['host', 'prefix', 'path', 'status', 'disabled', 'from', 'priority'];

    public function redirectRule(): HasOne
    {
        return $this->hasOne(RedirectRule::class);
    }

    public function redirectPrefixes(): HasMany
    {
        return $this->hasMany(RedirectPrefixes::class);
    }
}
