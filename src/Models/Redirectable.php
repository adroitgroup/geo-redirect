<?php

namespace AdroitGroup\GeoRedirect\Models;

use Illuminate\Database\Eloquent\Relations\MorphOne;

interface Redirectable
{
    public const REDIRECTABLE_CONTINENT = Continent::class;
    public const REDIRECTABLE_REGION = Region::class;
    public const REDIRECTABLE_SUBREGION = SubRegion::class;
    public const REDIRECTABLE_COUNTRY = Country::class;

    public function redirectRule(): MorphOne;
}