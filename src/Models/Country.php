<?php

namespace AdroitGroup\GeoRedirect\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Country extends Model implements Redirectable
{
    public $timestamps = false;

    public function redirectRule(): MorphOne
    {
        return $this->morphOne(RedirectRule::class, 'redirectable');
    }

    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }

    public function continent(): BelongsTo
    {
        return $this->belongsTo(Continent::class);
    }

    public function subRegion(): BelongsTo
    {
        return $this->belongsTo(SubRegion::class);
    }
}
