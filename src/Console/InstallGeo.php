<?php

namespace AdroitGroup\GeoRedirect\Console;

use AdroitGroup\GeoRedirect\Models\Continent;
use AdroitGroup\GeoRedirect\Models\Country;
use AdroitGroup\GeoRedirect\Models\Redirect;
use AdroitGroup\GeoRedirect\Models\Redirectable;
use AdroitGroup\GeoRedirect\Models\RedirectPrefixes;
use AdroitGroup\GeoRedirect\Models\RedirectRule;
use AdroitGroup\GeoRedirect\Models\Region;
use AdroitGroup\GeoRedirect\Models\SubRegion;
use AdroitGroup\GeoRedirect\Seeder\ContinentSeeder;
use AdroitGroup\GeoRedirect\Seeder\CountrySeeder;
use AdroitGroup\GeoRedirect\Seeder\RegionSeeder;
use AdroitGroup\GeoRedirect\Seeder\SubRegionSeeder;
use Illuminate\Console\Command;

class InstallGeo extends Command
{
    protected $signature = 'geo:install';
    protected $description = 'Install GEO basic functions, seeds';

    private const SEEDERS = [
        ContinentSeeder::class, RegionSeeder::class, SubRegionSeeder::class, CountrySeeder::class
    ];

    public function handle(): void
    {
        $this->info('Installing GEO seeds...');

        foreach (self::SEEDERS as $seeder) {
            app()->make($seeder)->run();
        }

        $initRedirects = config('geo_redirect.init', []);

        if (count($initRedirects) > 0) {
            $this->info('Inserting init redirects from config file...');
            $this->initRedirects($initRedirects);
        }

        $this->info('Installed GEO package');
    }

    private function initRedirects(array $initRedirects): void
    {
        $types = [
            'continent' => Continent::class,
            'region' => Region::class,
            'sub_region' => SubRegion::class,
            'country' => Country::class,
        ];

        if (\DB::getDriverName() !== 'sqlite') {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            \DB::table('redirect_rules')->truncate();
            \DB::table('redirect_prefixes')->truncate();
            \DB::table('redirects')->truncate();
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

        foreach ($types as $typeName => $typeClass) {
            $this->info('Populate: ' . $typeClass);
            if (isset($initRedirects[$typeName])) {
                $this->populate($initRedirects[$typeName], $typeClass);
            }
        }
    }

    private function populate(array $items, string $by): void
    {
        foreach ($items as $name => $toData) {
            $redirectable = $by::where(['name' => $name])->first();

            if ($redirectable === null) {
                $this->warn('[' . $by . '] Not found: ' . $name);
                dump('[' . $by . '] Not found: ' . $name);
                continue;
            }

            if (isset($toData['path'])) {
                $this->configureFrom($redirectable, '*', $toData, $name);
            } else {
                foreach ($toData as $from => $data) {
                    $this->configureFrom($redirectable, $from, $data, $name);
                }
            }
        }
    }

    private function configureFrom(Redirectable $redirectable, string $from, array $toData, string $name): void
    {
        if (! is_array($toData)) {
            $this->error('Invalid data in geo_redirect config file. Error on geo_redirect config file: ' . $name);
        } else {
            $this->info('Rule on [' . $name . ']: ' . ($toData['priority'] ?? 5));

            $this->createRedirectRule(
                $from,
                $toData['priority'] ?? 5,
                $redirectable,
                $toData['host'] ?? '',
                $toData['prefix'] ?? '',
                $toData['path'] ?? '',
                $toData['disabled'] ?? false
            );
        }
    }


    private function createRedirectRule(string $from,
                                        int $priority,
                                        Redirectable $redirectable,
                                        string $host,
                                        $prefix,
                                        string $path,
                                        bool $disabled = false): void
    {
        $prefixes = is_array($prefix) ? $prefix['default'] : $prefix;

        $redirect = Redirect::create([
            'from' => $from,
            'priority' => $priority,
            'host' => $host,
            'prefix' => $prefixes,
            'path' => $path,
            'status' => true,
            'disabled' => $disabled
        ]);

        $redirectRule = RedirectRule::create([
            'redirect_id' => $redirect->id,
            'redirectable_id' => $redirectable->id,
            'redirectable_type' => get_class($redirectable),
        ]);

        if (is_array($prefix)) {
            foreach ($prefix as $key => $item) {
                if ($key !== 'default') {
                    RedirectPrefixes::create([
                        'redirect_id' => $redirectRule->id,
                        'prefix' => $item,
                    ]);
                }
            }
        }
    }
}
