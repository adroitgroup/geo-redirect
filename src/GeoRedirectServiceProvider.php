<?php

namespace AdroitGroup\GeoRedirect;

use AdroitGroup\GeoRedirect\Console\InstallGeo;
use AdroitGroup\GeoRedirect\Http\Middleware\RedirectMiddleware;
use AdroitGroup\GeoRedirect\Services\GeoIdentifier;
use AdroitGroup\GeoRedirect\Services\GeoIdentifierInterface;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class GeoRedirectServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot(): void
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'geo-redirect');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'geo-redirect');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/geo_redirect.php' => config_path('geo_redirect.php'),
            ], 'config');

            $this->publishes([
                __DIR__.'/../database/migrations/' => database_path('/migrations')
            ], 'migrations');

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/geo-redirect'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/geo-redirect'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/geo-redirect'),
            ], 'lang');*/

            // Registering package commands.
            $this->commands([
                InstallGeo::class
            ]);
        }

        /** @var Router $router */
        $router = $this->app->make(Router::class);
        //$router->pushMiddlewareToGroup('web', RedirectMiddleware::class);
        $router->aliasMiddleware('geo-redirect', RedirectMiddleware::class);
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/geo_redirect.php', 'geo_redirect');

        // Register the main class to use with the facade
        $this->app->singleton('geo_redirect', function () {
            return new GeoRedirect;
        });

        $this->app->bind(GeoIdentifierInterface::class, function () {
            $dbPath = file_exists(config('geo_redirect.db.file'))
                ? config('geo_redirect.db.file')
                : __DIR__ . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . 'geolite2.mmdb';

            return new GeoIdentifier($dbPath);
        });
    }
}
