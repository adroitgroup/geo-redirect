<?php

namespace AdroitGroup\GeoRedirect\Services;

use AdroitGroup\GeoRedirect\Models\Redirect;
use AdroitGroup\GeoRedirect\Models\Redirectable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class GeoRedirectHandler
{
    private GeoIdentifierInterface $geoIdentifier;
    private CidrList $cidrList;

    private ?string $rewriteUrl = null;
    private array $rewriteUrlList = [];
    private bool $bypassPath = false;
    private ?string $preferredPath = null;
    private bool $disabled = false;
    private ?string $host = null;
    private ?string $view = null;
    private array $redirectPrefixes = [];

    /**
     * GeoRedirectHandler constructor.
     * @param GeoIdentifierInterface $geoIdentifier
     * @param CidrList $cidrList
     */
    public function __construct(GeoIdentifierInterface $geoIdentifier, CidrList $cidrList)
    {
        $this->geoIdentifier = $geoIdentifier;
        $this->cidrList = $cidrList;
    }

    public function check(Request $request): bool
    {
        /// $uri = preg_replace( "#^[^:/.]*[:/]+#i", "", $request->getUri()); // TODO: REGEX

        $identity = $this->geoIdentifier->identifyRequest($request);
        $fromHost = $request->getUri();

        if (($redirect = $this->hasCountryRestriction($identity, $fromHost)) !== null) {
            $this->generateRedirectUri($redirect, $request);
        }

        if (($redirect = $this->hasSubRegionRestriction($identity, $fromHost)) !== null) {
            $this->generateRedirectUri($redirect, $request);
        }

        if (($redirect = $this->hasRegionRestriction($identity, $fromHost)) !== null) {
            $this->generateRedirectUri($redirect, $request);
        }

        if (($redirect = $this->hasContinentRestriction($identity, $fromHost)) !== null) {
            $this->generateRedirectUri($redirect, $request);
        }

        if (count($this->rewriteUrlList) === 0) {
            return false;
        }

        krsort($this->rewriteUrlList);
        $finishedRewriteUrl = $this->rewriteUrlList[array_key_first($this->rewriteUrlList)];
        $finishedRewriteUrlData = $this->removeEmptyElements(explode('/', $finishedRewriteUrl));

        if (! config('geo_redirect.has_prefix', true) && count($request->segments()) > 1) {
            return false;
        }

        if (
            (count($request->segments()) > (config('geo_redirect.has_prefix', true) ? 2 : 1))
            && isset($finishedRewriteUrlData[0])
            && $finishedRewriteUrlData[0] === $request->segment(1)
        ) {
            return false;
        }

        if ($request->segment(1) !== null && $this->redirectPrefixes && in_array($request->segment(1), $this->redirectPrefixes, true)) {
            return false;
        }

        $finishedRewriteUrlDataArray = explode('/', $finishedRewriteUrl);

        if (
            count($request->segments()) === 1
            && isset($finishedRewriteUrlDataArray[1])
            && in_array($request->segments()[0], [
                'scb',
                'fsc',
                'fca',
                'fsca',
            ])
        ) {
            $finishedRewriteUrl = $request->segments()[0] . '/' . $finishedRewriteUrlDataArray[2];
        }

        $this->rewriteUrl = $finishedRewriteUrl;

        return true;
    }

    private function generateRedirectUri(Collection $redirects, Request $request): void
    {
        foreach ($redirects as $redirect) {
            $this->initRewriteUrl($redirect);
            $rewriteUrl = $this->rewriteUrl;

            $isPrefixSame = Str::startsWith($request->fullUrl(), $rewriteUrl);

            if (! $this->bypassPath && $this->preferredPath !== null) {
                $rewriteUrl .= ltrim(rtrim($this->preferredPath, '/'), '/');
            } else if (! $this->bypassPath || ! $isPrefixSame) {
                $rewriteUrl .= ltrim(rtrim($redirect->path, '/'), '/');
            }

            if ($redirect->status && $redirect->disabled) {
                $this->disabled = true;
            }

            if ($redirectPrefixes = $redirect->redirectPrefixes) {
                foreach ($redirectPrefixes as $redirectPrefix) {
                    $this->redirectPrefixes[] = $redirectPrefix->prefix;
                }
            }

            if ($redirect->redirectRule && $redirectableName = $redirect->redirectRule->redirectable->name) {
                $redirectConfig = array_column(config('geo_redirect.init'), $redirectableName);

                if ($redirectConfig && isset($redirectConfig[0]['view']) && ! ($redirect->status && $redirect->disabled)) {
                    if ($this->checkIpAddress($request)) {
                        continue;
                    }
                    $this->view = $redirectConfig[0]['view'];
                }
            }

            if ($request->getQueryString() !== null && strlen($request->getQueryString()) > 0) {
                $rewriteUrl .= '?' . $request->getQueryString();
            }

            if (isset ($this->rewriteUrlList[$redirect->priority])) {
                $redirect->priority--;
            }
            $this->rewriteUrlList[$redirect->priority] = $rewriteUrl;
        }
    }

    public function getUrl(): ?string
    {
        return $this->rewriteUrl;
    }

    private function hasContinentRestriction(GeoInfo $identity, string $fromHost = '*'): ?Collection
    {
        return $this->findRedirect(Redirectable::REDIRECTABLE_CONTINENT, $identity->getContinent()->id, $fromHost, $identity);
    }

    private function hasRegionRestriction(GeoInfo $identity, string $fromHost = '*'): ?Collection
    {
        return $this->findRedirect(Redirectable::REDIRECTABLE_REGION, $identity->getRegion()->id, $fromHost, $identity);
    }

    private function hasSubRegionRestriction(GeoInfo $identity, string $fromHost = '*'): ?Collection
    {
        return $this->findRedirect(Redirectable::REDIRECTABLE_SUBREGION, $identity->getSubRegion()->id, $fromHost, $identity);
    }

    private function hasCountryRestriction(GeoInfo $identity, string $fromHost = '*'): ?Collection
    {
        return $this->findRedirect(Redirectable::REDIRECTABLE_COUNTRY, $identity->getCountry()->id, $fromHost, $identity);
    }

    private function findRedirect(?string $redirectableType, ?string $redirectableId, ?string $fromHost, ?GeoInfo $identity): ?Collection
    {
        if ($redirectableId === null || $redirectableType === null) {
            return null;
        }

        if (! $fromHost) {
            $fromHost = '*';
        }

        $redirect = Redirect::whereHas('redirectRule', function ($q) use ($redirectableType, $redirectableId) {
            $q->where('redirectable_id', $redirectableId)
                ->where('redirectable_type', $redirectableType);
        });

        $redirects = $redirect->get();
        if ($redirects !== null && $redirects->count() > 0) {
            foreach ($redirects as $key => $redirect) {
                if ($redirect->from !== null) {
                    try {
                        if ($redirect->from === '*') {
                            $redirect->from = '.*';
                        }
                        if (preg_match('/' . $redirect->from . '/', $fromHost) === 0) {
                            unset($redirects[$key]);
                        }
                    } catch (\Exception $exception) {
                        Log::error('Geo-Redirect: Error while preg_match: /' . $redirect->from . '/' . ' , fromHost: ' . $fromHost);
                        // dd($exception);
                        // TODO: LOG
                    }
                }
            }
            $redirects = $redirects->sortKeys();
        }

        return $redirects;
    }

    /**
     * @param bool $bypassPath
     * @return GeoRedirectHandler
     */
    public function setBypassPath(bool $bypassPath): GeoRedirectHandler
    {
        $this->bypassPath = $bypassPath;

        return $this;
    }

    /**
     * @param string|null $preferredPath
     */
    public function setPreferredPath(?string $preferredPath): void
    {
        $this->preferredPath = $preferredPath;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @param $host
     * @return $this
     */
    public function setHost($host): GeoRedirectHandler
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @return string|null
     */
    public function getView(): ?string
    {
        return $this->view;
    }

    /**
     * @param Redirect $redirect
     */
    private function initRewriteUrl(Redirect $redirect): void
    {
        if ($redirect->prefix) {
            $this->rewriteUrl = rtrim($redirect->host, '/')
                . '/'
                . ltrim(rtrim($redirect->prefix, '/'), '/')
                . '/';
        } else {
            $this->rewriteUrl = rtrim($redirect->host, '/') . '/';
        }

        $this->setHost($redirect->host);
    }


    private function removeEmptyElements(iterable $array): array
    {
        $resultArray = [];

        foreach ($array as $item) {
            if ($item !== '' && $item !== null) {
                $resultArray[] = $item;
            }
        }

        return $resultArray;
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function checkIpAddress(Request $request): bool
    {
        $identify = $this->geoIdentifier->identifyRequest($request);
        $allowedIps = config('geo_redirect.allowed_ips', []);

        foreach ($allowedIps as $allowedIp) {
            $ipAddress = explode('/', $allowedIp);

            if (isset($ipAddress[0], $ipAddress[1])) {
                try {
                    $this->cidrList->addRanges([$allowedIp]);
                    return (bool)$this->cidrList->findRangeByIP($identify->getIp());
                } catch (\Exception $exception) {
                    return false;
                }
            }
        }

        return in_array($identify->getIp(), config('geo_redirect.allowed_ips', []), true);
    }

    public static function getUserIdentity()
    {
        try {
            $handler = app(GeoRedirectHandler::class);
            $request = request();
            $identity = $handler->geoIdentifier->identifyRequest($request);
            $fromHost = $request->getUri();

            if (($redirect = $handler->hasCountryRestriction($identity, $fromHost)) !== null) {
                $handler->generateRedirectUri($redirect, $request);
            }

            if (($redirect = $handler->hasSubRegionRestriction($identity, $fromHost)) !== null) {
                $handler->generateRedirectUri($redirect, $request);
            }

            if (($redirect = $handler->hasRegionRestriction($identity, $fromHost)) !== null) {
                $handler->generateRedirectUri($redirect, $request);
            }

            if (($redirect = $handler->hasContinentRestriction($identity, $fromHost)) !== null) {
                $handler->generateRedirectUri($redirect, $request);
            }

            krsort($handler->rewriteUrlList);
            $firstElement = array_key_first($handler->rewriteUrlList);

            if ($firstElement && isset($handler->rewriteUrlList[$firstElement])) {
                $uri = $handler->rewriteUrlList[$firstElement];
                $uriExplode = explode('/', $uri);
                $legalEntity = $uriExplode[1];
                $fullLanguage = $uriExplode[2];
                $languageExplode = explode('?', $fullLanguage);
                $language = $languageExplode[0];

                return [
                    'legal_entity' => $legalEntity,
                    'language' => $language,
                ];
            }

            return null;
        } catch (\Exception $exception) {
            return null;
        }
    }
}
