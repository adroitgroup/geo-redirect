<?php

namespace AdroitGroup\GeoRedirect\Services;

class CidrList
{
    /**
     * @var array
     */
    private array $ranges = [];

    /**
     * @param $ranges
     */
    public function addRanges($ranges): void
    {
        foreach ($ranges as $range) {
            $this->addRange($range);
        }

        $this->sortRanges();
    }

    /**
     * @param $ip
     * @return false|mixed|void
     */
    public function findRangeByIP($ip)
    {
        return $this->_findRangeByIP(ip2long($ip));
    }

    /**
     * @param $ip
     * @param null $start
     * @param null $end
     * @return false|mixed|void
     */
    protected function _findRangeByIP($ip, $start = NULL, $end = NULL)
    {
        if ($start > $end) {
            return false;
        }

        if (is_null($start)) {
            $start = 0;
        }

        if (is_null($end)) {
            $end = count($this->ranges) - 1;
        }

        $mid = (int)floor(($end + $start) / 2);

        switch ($this->inRange($ip, $this->ranges[$mid])) {
            case 0:
                return $this->ranges[$mid][2];
            case -1:
                return $this->_findRangeByIP($ip, $start, $mid - 1);
            case 1:
                return $this->_findRangeByIP($ip, $mid + 1, $end);
        }
    }

    /**
     * @param $range
     */
    protected function addRange($range): void
    {
        [$subnet, $bits] = explode('/', $range);
        $subnet = ip2long($subnet);
        $mask = -1 << (32 - $bits);

        $min = $subnet & $mask;
        $max = $subnet | ~$mask;

        $this->ranges[] = [$min, $max, $range];
    }

    protected function sortRanges(): void
    {
        usort($this->ranges, function ($a, $b) {
            $res = $a[0] - $b[0];

            switch ($res) {
                case 0:
                    return $a[1] - $b[1];
                default:
                    return $res;
            }
        });
    }

    /**
     * @param $ip
     * @param $range
     * @return int
     */
    protected function inRange($ip, $range): int
    {
        [$start, $end] = $range;

        if ($ip < $start) {
            return -1;
        }

        if ($ip > $end) {
            return 1;
        }

        return 0;
    }
}
