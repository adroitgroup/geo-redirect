<?php

namespace AdroitGroup\GeoRedirect\Services;

use AdroitGroup\GeoRedirect\Models\Continent;
use AdroitGroup\GeoRedirect\Models\Country;
use AdroitGroup\GeoRedirect\Models\Region;
use AdroitGroup\GeoRedirect\Models\SubRegion;
use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use Illuminate\Http\Request;
use MaxMind\Db\Reader\InvalidDatabaseException;

class GeoIdentifier implements GeoIdentifierInterface
{
    private Reader $geoIpReader;

    /**
     * GeoIdentifier constructor.
     * @param string $dbPath
     * @throws InvalidDatabaseException
     */
    public function __construct(string $dbPath)
    {
        $this->geoIpReader = new Reader($dbPath);
    }

    public function identify(string $ip): GeoInfo
    {
        $country = $this->findCountry($this->countryIsoCodeFromIp($ip));

        return $this->generateGeoInfo($ip, $country);
    }

    public function identifyRequest(Request $request): GeoInfo
    {
        $ip = $this->ipAddressFromRequest($request);

        return $this->identify($ip);
    }

    private function generateGeoInfo(string $ip, ?Country $country): GeoInfo
    {
        $geoInfo = new GeoInfo();

        return $geoInfo
            ->setIp($ip)
            ->setCountry($country ?? new Country())
            ->setContinent($country->continent ?? new Continent())
            ->setRegion($country->region ?? new Region())
            ->setSubRegion($country->subRegion ?? new SubRegion());
    }

    private function findCountry(?string $isoCode): ?Country
    {
        if ($isoCode === null) {
            return null;
        }

        return Country::where(['alpha_2' => $isoCode])->first();
    }

    private function countryIsoCodeFromIp(string $ip): ?string
    {
        try {
            return $this->geoIpReader->country($ip)->country->isoCode;
        } catch (AddressNotFoundException | InvalidDatabaseException $e) {
            return null;
        }
    }

    private function ipAddressFromRequest(Request $request): string
    {
        return $_SERVER["HTTP_CF_CONNECTING_IP"] ?? $_SERVER['HTTP_X_REAL_IP'] ?? $request->ip();
    }
}
