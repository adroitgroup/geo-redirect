<?php

namespace AdroitGroup\GeoRedirect\Services;

use Illuminate\Http\Request;

interface GeoIdentifierInterface
{
    public function identify(string $ip): GeoInfo;
    public function identifyRequest(Request $request): GeoInfo;
}
