<?php

namespace AdroitGroup\GeoRedirect\Services;

use AdroitGroup\GeoRedirect\Models\Continent;
use AdroitGroup\GeoRedirect\Models\Country;
use AdroitGroup\GeoRedirect\Models\Region;
use AdroitGroup\GeoRedirect\Models\SubRegion;

class GeoInfo
{
    private Continent $continent;
    private Region $region;
    private ?SubRegion $subRegion;
    private Country $country;

    private string $ip;

    /**
     * @return Continent
     */
    public function getContinent(): Continent
    {
        return $this->continent;
    }

    /**
     * @param Continent $continent
     * @return GeoInfo
     */
    public function setContinent(Continent $continent): GeoInfo
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * @return Region
     */
    public function getRegion(): Region
    {
        return $this->region;
    }

    /**
     * @param Region $region
     * @return GeoInfo
     */
    public function setRegion(Region $region): GeoInfo
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return SubRegion
     */
    public function getSubRegion(): ?SubRegion
    {
        return $this->subRegion;
    }

    /**
     * @param ?SubRegion $subRegion
     * @return GeoInfo
     */
    public function setSubRegion(?SubRegion $subRegion): GeoInfo
    {
        $this->subRegion = $subRegion;

        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return GeoInfo
     */
    public function setCountry(Country $country): GeoInfo
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return GeoInfo
     */
    public function setIp(string $ip): GeoInfo
    {
        $this->ip = $ip;
        
        return $this;
    }
}
