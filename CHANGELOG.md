# Changelog

All notable changes to `geo-redirect` will be documented in this file

## [1.1.0] - 2021-03-28

### Added
- geo_redirect_bypass cookie
- geo_preferred_path cookie
- geo_path_bypass cookie

### Changed
- Database structure transformed
- Refactored geo redirect logic
- GEO config structure

### Fixed
- Fix middleware redirect condition

## [1.0.0] - 2021-03-19

- Initial release
