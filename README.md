# GEO-REDIRECT

> This package redirects the user to a route based on geolocation

## Installation

You can install the package via composer:

```bash
composer require adroitgroup/geo-redirect
```

### Publish config:

```bash
php artisan vendor:publish --provider="AdroitGroup\GeoRedirect\GeoRedirectServiceProvider" --tag="config"
```

### Publish migrations:

```bash
php artisan vendor:publish --provider="AdroitGroup\GeoRedirect\GeoRedirectServiceProvider" --tag="migrations"
```

## Usage

Simple

``` php
Route::get('/', function () {
    //
})->middleware('geo-redirect');
```

Group

``` php
use AdroitGroupGeoRedirect\Http\Middleware\RedirectMiddleware;

Route::middleware([RedirectMiddleware::class])->group(function () {
    Route::get('/', function () {
        //
    });
});
```

RouteServiceProvider

``` php
Route::middleware(['web', 'geo-redirect'])
    ->namespace($this->namespace)
    ->group(base_path('routes/web.php'));
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
