<?php

// continent => [from => [continent_name => 'to' ...]]
// region => [from => [region_name => 'to' ...]]
// sub_region => [from => [sub_region_name => 'to' ...]]
// country => [from => [country_name => 'to' ...]]

return [
    'has_prefix' => true,
    'db' => [
        'file' => storage_path('app/geoIP2/GeoLite2-Country.mmdb'),
    ],
    'deny_hosts' => [
        //'infinox.adroitgroup.io',
        //'infinoxcom.adroitgroup.io'
    ],
    'cookies' => [
        'geo_redirect_bypass' => [
            'affid',
            'ip_bypass',
        ]
    ],
    'allowed_paths' => [
        '/.*/',
    ],
    'init' => [
        'continent' => [
            'Africa' => [
                'infinox-zh.com' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'zh_cn', 'priority' => 10],
                'no_redirect' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'zh_cn', 'priority' => 7],
                '.*' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            ],
            'Europe' => [
                'infinox-zh.com' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'zh_cn', 'priority' => 10],
                'no_redirect' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'zh_cn', 'priority' => 7],
                '.*' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            ],
            'Asia' => [
                'infinox-zh.com' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'zh_cn', 'priority' => 10],
                'no_redirect' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'zh_cn', 'priority' => 7],
                '.*' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            ],
        ],
        'region' => [
            'Western Asia' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'], // Western Asia = All other ME countries
            'Latin America and the Caribbean' => ['host' => '/', 'prefix' => 'scb', 'path' => 'es'],
        ],
        'country' => [
            // SCB
            'Ghana' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Nigeria' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Kenya' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Mozambique' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Botswana' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Namibia' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Tanzania, United Republic of' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Zambia' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'South Africa' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Egypt' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'],
            'Morocco' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'],
            'Algeria' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'],
            'United Arab Emirates' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'],
            'Qatar' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'],
            'Saudi Arabia' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'],
            'Bahrain' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'],
            'Kuwait' => ['host' => '/', 'prefix' => 'scb', 'path' => 'ar'],
            'Mexico' => ['host' => '/', 'prefix' => 'scb', 'path' => 'es'],
            'Peru' => ['host' => '/', 'prefix' => 'scb', 'path' => 'es'],
            'Chile' => ['host' => '/', 'prefix' => 'scb', 'path' => 'es'],
            'Argentina' => ['host' => '/', 'prefix' => 'scb', 'path' => 'es'],
            'Colombia' => ['host' => '/', 'prefix' => 'scb', 'path' => 'es'],
            'Uruguay' => ['host' => '/', 'prefix' => 'scb', 'path' => 'es'],
            'Brazil' => ['host' => '/', 'prefix' => 'scb', 'path' => 'pt'],
            'United Kingdom of Great Britain and Northern Ireland' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],
            'Spain' => ['host' => '/', 'prefix' => 'scb', 'path' => 'es'],
            'Portugal' => ['host' => '/', 'prefix' => 'scb', 'path' => 'pt'],
            'India' => ['host' => '/', 'prefix' => 'scb', 'path' => 'en'],

            // FSC
            'Thailand' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'th'],
            'Malaysia' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'my'],
            'Japan' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'jp'],
            'Vietnam' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'vn'],
            'Indonesia' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'id'],
            'Korea, Republic of' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'kr'],
            'China' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'cns'],
            'Taiwan, Province of China' => ['host' => '/', 'prefix' => 'fsc', 'path' => 'cnt'],
            //'Hong Kong' => '#',
        ]
    ]
];
