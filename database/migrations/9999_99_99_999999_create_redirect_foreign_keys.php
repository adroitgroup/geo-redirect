<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedirectForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('redirect_rules', function (Blueprint $table) {
            $table->foreign('redirect_id')->references('id')->on('redirects');
        });

        Schema::table('countries', function (Blueprint $table) {
            //$table->foreign('continent_id')->references('id')->on('continents');
            //$table->foreign('region_id')->references('id')->on('regions');
            //$table->foreign('sub_region_id')->references('id')->on('sub_regions');
        });

        Schema::table('redirect_prefixes', function (Blueprint $table) {
            $table->foreign('redirect_id')->references('id')->on('redirects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('redirect_rules', function (Blueprint $table) {
            $table->dropForeign(['redirect_id']);
        });

        Schema::table('countries', function (Blueprint $table) {
            //$table->dropForeign(['continent_id', 'region_id', 'sub_region_id']);
        });

        Schema::table('redirect_prefixes', function (Blueprint $table) {
            $table->dropForeign(['redirect_id']);
        });
    }
}
