<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFromToRedirectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('redirects', function (Blueprint $table) {
            $table->string('from')->default('.*');
            $table->integer('priority')->default(5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('redirects', function($table) {
            $table->dropColumn('from');
            $table->dropColumn('priority');
        });
    }
}
