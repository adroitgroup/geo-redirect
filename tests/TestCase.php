<?php

namespace AdroitGroup\GeoRedirect\Tests;

use AdroitGroup\GeoRedirect\GeoRedirectServiceProvider;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Orchestra\Testbench\TestCase as OriginalTestCase;

class TestCase extends OriginalTestCase
{
    protected function getPackageProviders($app): iterable
    {
        return [GeoRedirectServiceProvider::class];
    }

    protected function getEnvironmentSetUp($app): void
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testing');
        $app['config']->set('database.connections.testing', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }

    protected function disableCookiesEncryption(string $name): self
    {
        $this->app->resolving(EncryptCookies::class,
            function ($object) use ($name) {
                $object->disableFor($name);
            });

        return $this;
    }
}
