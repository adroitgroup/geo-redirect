<?php

namespace AdroitGroup\GeoRedirect\Tests\Feature;

use AdroitGroup\GeoRedirect\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConfigReadCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_simple_request(): void
    {
        // GIVEN
        $count = $this->countingRules(config('geo_redirect.init.continent', []))
            + $this->countingRules(config('geo_redirect.init.region', []))
            + $this->countingRules(config('geo_redirect.init.sub_region', []))
            + $this->countingRules(config('geo_redirect.init.country', []));

        // THEN
        \Artisan::call('geo:install');

        // WHEN
        $this->assertDatabaseCount('redirect_rules', $count);
        $this->assertDatabaseCount('redirects', $count);
    }

    private function countingRules(array $rules): int
    {
        $count = 0;

        foreach ($rules as $rule) {
            if (isset($rule['host'])) {
                $count++;
            } else {
                $count += count($rule);
            }
        }

        return $count;
    }
}
