<?php

namespace AdroitGroup\GeoRedirect\Tests\Feature;

use AdroitGroup\GeoRedirect\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class RedirectTest extends TestCase
{
    use RefreshDatabase;

    public function test_simple_request(): void
    {
        // GIVEN
        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));

        // WHEN
        $response = $this->call('GET','test/route', [], [], [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertStatus(302);
    }

    public function test_simple_request_bypass_redirect(): void
    {
        // GIVEN
        $geoRedirectByPass = \Config::get('geo_redirect.cookies.geo_redirect_bypass');

        foreach ($geoRedirectByPass as $byPass) {
            $this->disableCookiesEncryption($byPass);
        }

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = array_fill_keys(\Config::get('geo_redirect.cookies.geo_redirect_bypass'), true);

        // WHEN
        $response = $this->call('GET','test/route', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertOk();
    }

    // Language changed (cookie set), should redirect with language
    public function test_prefix_changed_should_redirect_strict_prefix_1(): void
    {
        // GIVEN
        $this->disableCookiesEncryption('geo_path_bypass');

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = ['geo_path_bypass' => 1];

        // WHEN
        $response = $this->call('GET','fsc/ar', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);
        //dd($response->headers->get('location'));

        // THEN
        $response->assertRedirect('http://localhost/scb/ar');
    }

    // Language changed (cookie set), should redirect with language
    public function test_prefix_changed_should_redirect_strict_prefix_2(): void
    {
        // GIVEN
        $this->disableCookiesEncryption('geo_path_bypass');

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = ['geo_path_bypass' => 1];

        // WHEN
        $response = $this->call('GET','fsc/en', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);
        //dd($response->headers->get('location'));

        // THEN
        $response->assertRedirect('http://localhost/scb/ar');
    }

    public function test_no_redirect_because_same_prefix_in_config_and_exactUri(): void
    {
        // GIVEN
        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = [];

        // WHEN
        $response = $this->call('GET','scb/en', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertStatus(200);
        //$response->assertRedirect('http://localhost/scb/ar');
    }

    public function test_prefix_path_changed_cause_bypassed_with_cookie_should_no_redirect(): void
    {
        // GIVEN
        $this->disableCookiesEncryption('geo_path_bypass');

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = ['geo_path_bypass' => 1];

        // WHEN
        $response = $this->call('GET','scb/uk', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertOk();
    }

    public function test_full_bypass_redirect(): void
    {
        // GIVEN
        $geoRedirectByPass = \Config::get('geo_redirect.cookies.geo_redirect_bypass');

        foreach ($geoRedirectByPass as $byPass) {
            $this->disableCookiesEncryption($byPass);
        }

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = array_fill_keys(\Config::get('geo_redirect.cookies.geo_redirect_bypass'), \Str::random(20));

        // WHEN
        $response = $this->call('GET','fsc/en', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertOk();
    }

    public function test_without_prefix_and_path_should_redirect_by_config_without_path_bypass(): void
    {
        // GIVEN
        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = [];

        // WHEN
        $response = $this->call('GET','/', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertRedirect('http://localhost/scb/ar');
    }

    public function test_without_prefix_and_path_should_redirect_by_config_with_path_bypass(): void
    {
        // GIVEN
        $this->disableCookiesEncryption('geo_path_bypass');

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = ['geo_path_bypass' => 1];

        // WHEN
        $response = $this->call('GET','/', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertRedirect('http://localhost/scb/ar');
    }

    public function test_preferred_path_without_bypass_path(): void
    {
        // GIVEN
        $this->disableCookiesEncryption('geo_preferred_path');

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = ['geo_preferred_path' => 'uk'];

        // WHEN
        $response = $this->call('GET','/fsc/en', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertRedirect('http://localhost/scb/uk');
    }

    // For example: Language change
    public function test_preferred_path_with_bypass_path(): void
    {
        // GIVEN
        $this->disableCookiesEncryption('geo_preferred_path');
        $this->disableCookiesEncryption('geo_path_bypass');

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = ['geo_preferred_path' => 'uk', 'geo_path_bypass' => 1];

        // WHEN
        $response = $this->call('GET','/scb/en', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertOk();
    }

    public function test_preferred_path_without_bypass_path_with_wrong_prefix(): void
    {
        // GIVEN
        $this->disableCookiesEncryption('geo_preferred_path');

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = ['geo_preferred_path' => 'uk'];

        // WHEN
        $response = $this->call('GET','/fsc/en', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertRedirect('http://localhost/scb/uk');
    }

    // For example: Language change
    public function test_preferred_path_with_bypass_path_with_wrong_prefix(): void
    {
        // GIVEN
        $this->disableCookiesEncryption('geo_preferred_path');
        $this->disableCookiesEncryption('geo_path_bypass');

        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        $cookies = ['geo_preferred_path' => 'uk', 'geo_path_bypass' => 1];

        // WHEN
        $response = $this->call('GET','/fsc/en', [], $cookies, [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertRedirect('http://localhost/scb/ar');
    }

    public function test_custom_host_redirect(): void
    {
        // GIVEN
        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));

        // WHEN
        $response = $this->call('GET','test/route', [], [], [], ['REMOTE_ADDR' => '5.83.112.23']);

        // THEN
        $response->assertRedirect('http://localhost/scb/ar');
        $response->assertStatus(302);
    }

    public function test_disable_country(): void
    {
        // GIVEN
        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));
        View::getFinder()->prependLocation(__DIR__.'/../Resources/views/');

        // WHEN
        $response = $this->call('GET','fsc/en', [], [], [], ['REMOTE_ADDR' => '103.27.203.97']);

        // THEN
        $response->assertViewIs('errors.GK_Hong_Kong');
        $response->assertStatus(200);
    }

    public function test_full_disable_country(): void
    {
        // GIVEN
        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));

        // WHEN
        $response = $this->call('GET','fsc/en', [], [], [], ['REMOTE_ADDR' => '103.27.203.97']);

        // THEN
        $response->assertStatus(422);
    }

    public function test_allow_ip_address(): void
    {
        // GIVEN
        \Artisan::call('geo:install');
        Route::middleware('web')->group(__DIR__ . ('/../Routes/test-routes-1.php'));

        // WHEN
        $response = $this->call('GET','fsc/en', [], [], [], ['REMOTE_ADDR' => '103.27.203.99']);

        // THEN
        $response->assertStatus(200);
    }
}
