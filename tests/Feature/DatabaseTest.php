<?php

namespace AdroitGroup\GeoRedirect\Tests\Feature;

use AdroitGroup\GeoRedirect\Models\Continent;
use AdroitGroup\GeoRedirect\Models\Country;
use AdroitGroup\GeoRedirect\Models\Redirect;
use AdroitGroup\GeoRedirect\Models\Redirectable;
use AdroitGroup\GeoRedirect\Models\RedirectRule;
use AdroitGroup\GeoRedirect\Models\Region;
use AdroitGroup\GeoRedirect\Models\SubRegion;
use AdroitGroup\GeoRedirect\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatabaseTest extends TestCase
{
    use RefreshDatabase;

    public function test_db_seeds_by_command_should_seed_database(): void
    {
        \Artisan::call('geo:install');

        $this->assertDatabaseHas('continents', ['name' => 'Europe']);
        $this->assertDatabaseHas('regions', ['name' => 'Northern Europe']);
        $this->assertDatabaseHas('sub_regions', ['name' => 'Eastern Africa']);
        $this->assertDatabaseHas('countries', ['name' => 'Hungary']);
    }

    public function test_db_seeds_should_insert_one_time(): void
    {
        \Artisan::call('geo:install');
        \Artisan::call('geo:install');
        \Artisan::call('geo:install');

        $this->assertCount(1, Country::where(['name' => 'Hungary'])->get());
        $this->assertCount(1, Region::where(['name' => 'Northern Europe'])->get());
        $this->assertCount(1, SubRegion::where(['name' => 'Eastern Africa'])->get());
        $this->assertCount(1, Continent::where(['name' => 'Europe'])->get());
    }

    public function test_morph()
    {
        // GIVEN
        \Artisan::call('geo:install');
        $redirectRule = RedirectRule::create([
            'redirect_id' => Redirect::insert(['host' => 'localhost', 'prefix' => '', 'path' => 'random']),
            'redirectable_id' => Country::first()->id,
            'redirectable_type' => Redirectable::REDIRECTABLE_COUNTRY
        ]);

        // WHEN
        $redirectRuleMorphed = $redirectRule->redirectable;

        // THEN
        $this->assertInstanceOf(Country::class, $redirectRuleMorphed);
    }
}
