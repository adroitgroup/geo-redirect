<?php

namespace AdroitGroup\GeoRedirect\Tests\Feature;

use AdroitGroup\GeoRedirect\Services\GeoIdentifierInterface;
use AdroitGroup\GeoRedirect\Services\GeoInfo;
use AdroitGroup\GeoRedirect\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GeoIp2\Database\Reader;

class GeoMmdbTest extends TestCase
{
    use RefreshDatabase;

    public function test_access(): void
    {
        // GIVEN
        $randomIp = '51.195.30.180'; // French IP address
        $geoLiteFilePath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'geolite2.mmdb';

        // WHEN
        $reader = new Reader($geoLiteFilePath);
        $country = $reader->country($randomIp);

        // THEN
        $this->assertEquals('FR', $country->country->isoCode);
    }

    public function test_service(): void
    {
        // GIVEN
        \Artisan::call('geo:install');
        $randomIp = '51.195.30.180'; // French IP address

        // WHEN
        $service = $this->app->make(GeoIdentifierInterface::class);
        $geoInfo = $service->identify($randomIp);

        // THEN
        $this->assertInstanceOf(GeoInfo::class, $geoInfo);
    }
}
