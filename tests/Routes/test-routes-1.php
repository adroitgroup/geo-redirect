<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'geo-redirect'], function () {
    Route::get('/', function () {});

    Route::get('test/route', function () {});
    Route::get('test/other/route', function () {});

    Route::get('posts/{id}', function () {});
    Route::get('articles/{slug}', function () {});
    Route::get('users/{id}', function () {});


    Route::get('scb/uk', function () {});
    Route::get('scb/en', function () {});
    Route::get('fsc/ar', function () {});
    Route::get('fsc/en', function () {});
});
